# Binary Search Tree

##### Official documentaion
    
- [TreeMap](https://docs.oracle.com/javase/7/docs/api/java/util/TreeMap.html)
- [TreeSet](https://docs.oracle.com/javase/7/docs/api/java/util/TreeSet.html)

##### Docs

- [ Sedgewick and Kevin Wayne](https://algs4.cs.princeton.edu/32bst/)  
- [Sedgewick and Kevin Wayne (Presentation)](https://algs4.cs.princeton.edu/lectures/32BinarySearchTrees.pdf)  

##### Implementation

- [BST](https://algs4.cs.princeton.edu/32bst/BST.java.html)    