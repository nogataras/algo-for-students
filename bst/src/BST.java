import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class BST<Key extends Comparable<Key>> implements Iterable<Key> {

    public class Node<Key> {
        public Key key;
        Node<Key> left, right;
        int size, height;

        Node(Key key) {
            this.key = key;
            size = 1;
            height = 0;
        }

        public void update(){
            int leftHeight = left == null ? 0 : left.height;
            int rightHeight = right == null ? 0 : right.height;
            height = Math.max(leftHeight, rightHeight) + 1;

            int leftSize = left == null ? 0 : left.size;
            int rightSize = right == null ? 0 : right.size;
            size = leftSize + rightSize + 1;
        }
    }

    private Node<Key> root;

    public BST(){
        root = null;
    }

    public boolean isEmpty(){
        return size() == 0;
    }

    public int size(){
        return root == null ? 0 : root.size;
    }

    public boolean contains(Key key){
        return contains(key, root);
    }

    private boolean contains(Key key, Node<Key> root){
        if (root == null) {
            return false;
        }

        int cmp = root.key.compareTo(key);
        if (cmp == 0) {
            return true;
        } else if (cmp < 0) {
            return contains(key, root.left);
        } else {
            return contains(key, root.right);
        }
    }

    public void put(Key key){
        root = put(key, root);
    }

    private Node<Key> put(Key key, Node<Key> root){
        if (root == null) {
            return new Node<Key>(key);
        }

        int cmp = root.key.compareTo(key);
        if (cmp == 0) {
            return root;
        } else if (cmp > 0) {
            Node<Key> left = put(key, root.left);
            root.left = left;
        } else {
            Node<Key> right = put(key, root.right);
            root.right = right;
        }
        root.update();
        return root;
    }

    public void delete(Key key){
        //TODO: Implement
    }

    public int height(){
        return root.height;
    }


    @Override
    public Iterator<Key> iterator() {
        ArrayList<Key> order = new ArrayList<>();
        inOrder(root, order);
        return order.iterator();
    }

    private void inOrder(Node<Key> root, List<Key> order){

        if(root == null) return;

        inOrder(root.left, order);
        order.add(root.key);
        inOrder(root.right, order);
    }

    public static void main(String[] args) {

        BST<Integer> bst = new BST<>();

        System.out.println(bst.isEmpty());


        for (int i = 0; i < 1000_000; i++) {
            bst.put(i);
        }
        /*bst.put(10);
        bst.put(12);
        bst.put(25);
        bst.put(34);
        bst.put(11);
        bst.put(12);
        bst.put(29);
        bst.put(75);
        bst.put(59);
        bst.put(33);*/

        System.out.println(bst.isEmpty());
        System.out.println(bst.size());

        System.out.println(bst.contains(28));

        bst.put(28);

        System.out.println(bst.contains(28));

        for(int key : bst){
            System.out.println(key);
        }

        bst.delete(11);
        System.out.println(bst.contains(11));
    }

}
