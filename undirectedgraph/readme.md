# Graphs 1. Undirected graph

##### Graph Representation
    
- [Basic](https://habrahabr.ru/post/65367/)
- [Undirected Graph](https://algs4.cs.princeton.edu/lectures/41UndirectedGraphs.pdf)
- [Graph representation](http://www.geeksforgeeks.org/graph-and-its-representations/)
- [Video1](https://www.youtube.com/watch?v=fMuTwUYhxeY)
- [Video2](https://www.youtube.com/watch?v=WtfGRS1BsBI)

##### Graph Traversal (Depth First Search & Breadth-First Search)
- [BFS&DFS 1](http://www.cs.cmu.edu/~ckingsf/class/02713-s13/lectures/lec07-dfsbfs.pdf)
- [BFS&DFS 2](https://www.slideshare.net/mkurnosov/8-32922461)