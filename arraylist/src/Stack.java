
public interface Stack<E> extends Iterable<E> {

    /**
     * Get size of the stack
     *
     * @return the size of elemnts
     */
    int size();

    /**
     * Check if the list is empty
     *
     * @return true if empty else false
     */
    boolean isEmpty();

    /**
     * Push element to the top of the stack
     *
     * @param element - element to be pushed
     */
    void push(E element);

    /**
     * Retrive the top element from  the stack.
     * After retrieving the element is removed from the top.
     *
     * @return - retrieved elemnt
     */
    E pop();

    /**
     * Get top element of the stack.
     * The element isn't removed from the top.
     *
     * @return
     */
    E peek();



}
