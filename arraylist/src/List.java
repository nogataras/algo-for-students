public interface List<E> extends Iterable<E>  {

    /**
     * Get size of the list
     *
     * @return the size of elemnts
     */
    int size();

    /**
     * Check if the list is empty
     *
     * @return true if empty else false
     */
    boolean isEmpty();

    /**
     * Remove an element from the list
     *
     * @param ind index of the element to be removed
     */
    void remove(int ind);

    /**
     * add the element at given position
     *
     * @param ind - index for the insertion
     * @param element - element to be inserted
     */
    void add(int ind, E element);

    /**
     * Add the element at the end of the list
     *
     * @param element - elemnt ot be inserted
     */
    void add(E element);

    /**
     * Get the element at the given position
     *
     * @param ind - index of the elemetn to return
     */
    E get(int ind);

    /**
     * Set an element at the given position
     *
     * @param ind - index of the element
     * @param element - the element
     */
    void set(int ind,E element);
}
