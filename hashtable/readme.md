# HashTable

##### Official documentaion
    
- [HashMap](https://docs.oracle.com/javase/7/docs/api/java/util/HashMap.html)
- [HashSet](https://docs.oracle.com/javase/7/docs/api/java/util/HashSet.html)

##### Docs

- [ Sedgewick and Kevin Wayne](https://algs4.cs.princeton.edu/34hash/)  
- [Sedgewick and Kevin Wayne (Presentation)](https://algs4.cs.princeton.edu/lectures/34HashTables.pdf)  

##### Implementation

- [SeparateChainingHashTable](https://algs4.cs.princeton.edu/34hash/SeparateChainingHashST.java.html)    
- [LinearProbingHashTable](https://algs4.cs.princeton.edu/34hash/LinearProbingHashST.java.html)    