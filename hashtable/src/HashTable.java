public class HashTable<Key, Value> {

    public HashTable(){
        //TODO: Implement
    }

    public boolean isEmpty(){
        //TODO: Implement
        return false;
    }

    public int size(){
        //TODO: Implement
        return 0;
    }

    public boolean contains(Key key){
        return false;
    }

    public void put(Key key, Value value){
        //TODO: Implement
    }

    public Value get(Key key){
        //TODO: Implement
        return null;
    }

    public void delete(Key key){
        //TODO: Implemnet
    }

    public static void main(String[] args) {

        HashTable<String, Integer> hashTable = new HashTable<>();

        System.out.println(hashTable.isEmpty());

        hashTable.put("a",10);
        hashTable.put("b",10);
        hashTable.put("c",20);
        hashTable.put("q",30);
        hashTable.put("e",15);
        hashTable.put("z",15);
        hashTable.put("g",29);
        hashTable.put("n",75);
        hashTable.put("l",59);
        hashTable.put("3",33);

        System.out.println(hashTable.isEmpty());
        System.out.println(hashTable.size());

        System.out.println(hashTable.contains("o"));

        hashTable.put("o", 31);

        System.out.println(hashTable.contains("o"));

        System.out.println(hashTable.get("o"));
        System.out.println(hashTable.get("b"));
        System.out.println(hashTable.get("c"));
        System.out.println(hashTable.get("g"));
        System.out.println(hashTable.get("n"));
        System.out.println(hashTable.get("l"));


        hashTable.delete("o");
        System.out.println(hashTable.contains("o"));
    }




}
